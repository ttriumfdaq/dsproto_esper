/********************************************************************\
Example period frontend
 Thomas Lindner (TRIUMF)
 \********************************************************************/

#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "midas.h"
#include "mfe.h"
#include "EsperComm.h"

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char const *frontend_name = "feChronoEsper";
/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 3 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) --- not really used here */
INT max_event_size_frag = 2 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 20 * 1000000;
HNDLE hSet;

extern HNDLE hDB;

Esper::EsperComm *gEsperComm;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/

EQUIPMENT equipment[] = {

{ "Chrono_Esper", /* equipment name */
{ 101, 2, /* event ID, trigger mask */
"SYSTEM", /* event buffer */
EQ_PERIODIC, /* equipment type */
LAM_SOURCE(0, 0), /* event source crate 0, all stations */
"MIDAS", /* format */
TRUE, /* enabled */
RO_ALWAYS | RO_ODB, /* read only when running */
1000, /* poll for 1000ms */
0, /* stop run after this event limit */
0, /* number of sub events */
0, /* don't log history */
"", "", "", }, read_trigger_event, /* readout routine */
}, { "" } };

/********************************************************************\
              Callback routines for system transitions

 These routines are called whenever a system transition like start/
 stop of a run occurs. The routines are called on the following
 occations:

 frontend_init:  When the frontend program is started. This routine
 should initialize the hardware.

 frontend_exit:  When the frontend program is shut down. Can be used
 to releas any locked resources like memory, commu-
 nications ports etc.

 begin_of_run:   When a new run is started. Clear scalers, open
 rungates, etc.

 end_of_run:     Called on a request to stop a run. Can send
 end-of-run event and close run gates.

 pause_run:      When a run is paused. Should disable trigger events.

 resume_run:     When a run is resumed. Should enable trigger events.
 \********************************************************************/

std::vector<std::string> gModules;
Esper::EsperNodeData gData;

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init() {

   set_equipment_status(equipment[0].name, "Initializing...", "#FFFF00");
   printf("<<< Begin of Init\n");

   gEsperComm = new Esper::EsperComm("chronobox");

   // Get the list of modules
   gEsperComm->Open();
   gEsperComm->GetModules(&gModules);
   gEsperComm->Close();

   Esper::EsperNodeData data;

   // Get the set of JSON entry keys

   gEsperComm->Open();
   KOtcpError err = gEsperComm->ReadVariables(gModules[2], &gData[gModules[2]]);
   Esper::EsperModuleData mdata = gData[gModules[2]];
   gEsperComm->Close();

   std::vector<std::string> names;
   for (std::map<std::string, int>::iterator it = gData[gModules[2]].i.begin(); it != gData[gModules[2]].i.end(); ++it) {
      names.push_back(it->first);
   }

   for (std::map<std::string, std::vector<int>>::iterator it = gData[gModules[2]].ia.begin(); it != gData[gModules[2]].ia.end(); ++it) {
      for (unsigned int i = 0; i < it->second.size(); i++) {
         char tmp3[10];
         sprintf(tmp3, "%s[%i]", it->first.c_str(), i);
         names.push_back(std::string(tmp3));
      }
   }

   for (unsigned int i = 0; i < names.size(); i++) {
      std::cout << names[i] << std::endl;
   }

   HNDLE hkey;
   KEY key;
   int status;

   // Create "Settings/Names ESPI" variable if needed.
   char str[256];
   sprintf(str, "/Equipment/Chrono_Esper/Settings");
   status = db_find_key(hDB, 0, str, &hkey);

   if (status == DB_NO_KEY) {
      db_create_key(hDB, hkey, str, TID_KEY);
   } else if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find ODB key '%s'", str);
      return DB_NO_KEY;
   }

   sprintf(str, "/Equipment/Chrono_Esper/Settings/Names ESPI");
   status = db_find_key(hDB, 0, str, &hkey);

   if (status == DB_NO_KEY) {
      db_create_key(hDB, hkey, str, TID_STRING);
   } else if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find ODB key '%s'", str);
      return DB_NO_KEY;
   }

   status = db_find_key(hDB, 0, str, &hkey);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find ODB key '%s'", str);
      return DB_NO_KEY;
   }

   status = db_get_key(hDB, hkey, &key);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to get ODB key '%s'", str);
      return DB_NO_KEY;
   }

   if (key.num_values != (int) names.size() && names.size()) {
      db_set_num_values(hDB, hkey, names.size());
   }

   // Set the string descriptor for each variable.
   char name[32];

   for (unsigned int i = 0; i < names.size(); i++) {
      sprintf(name, "%s", names[i].substr(0,31).c_str());
      int size = sizeof(name);
      db_set_data_index(hDB, hkey, &name, size, i, TID_STRING);
   }

   set_equipment_status(equipment[0].name, "Initialized", "#00FF00");

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit() {

   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
// Upon run stasrt, read ODB settings and write them to DCRC
INT begin_of_run(INT run_number, char *error) {

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error) {
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error) {
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error) {
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop() {
   /* if frontend_call_loop is true, this routine gets called when
    the frontend is idle or once between every event */
   usleep(1000);
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************
 Readout routines for different events

 \********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
// Not currently used for DCRC readout
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
 is available. If test equals TRUE, don't return. The test
 flag is used to time the polling */
{
  int i;
  
  for (i = 0; i < count; i++) {
    //      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);
    
    //      if (lam & LAM_SOURCE_STATION(source))
    if (!test)
      return 1;
  }
  
  usleep(1000);
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
// This is not currently used by the DCRC readout
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off) {

   printf("Reading event\n");
   /* init bank structure */
   bk_init32(pevent);

   //  DWORD *pdata;
   // bk_create(pevent, "ESPI", TID_DWORD, (void **)&pdata);
   //  *pdata++ = 0xdeadbeef;
   //bk_close(pevent, pdata);

   gEsperComm->Open();

   std::cout << "Module: " << gModules[2] << std::endl;
   KOtcpError err = gEsperComm->ReadVariables(gModules[2], &gData[gModules[2]]);
   //  std::cout << err.error << " " << err.xerrno << " " << err.message << std::endl;
   Esper::EsperModuleData mdata = gData[gModules[2]];
   std::cout << "mdata size: " << gData[gModules[2]].ia.size() << " " << gData[gModules[2]].i.size() << " " << gData[gModules[2]].da.size() << " " << gData[gModules[2]].d.size() << std::endl;

   DWORD *pdata;
   bk_create(pevent, "ESPI", TID_DWORD, (void **) &pdata);
   for (std::map<std::string, int>::iterator it = gData[gModules[2]].i.begin(); it != gData[gModules[2]].i.end(); ++it) {
      *pdata++ = (unsigned int) it->second;
   }
   for (std::map<std::string, std::vector<int>>::iterator it = gData[gModules[2]].ia.begin(); it != gData[gModules[2]].ia.end(); ++it) {
      for (unsigned int i = 0; i < it->second.size(); i++) {
         *pdata++ = (unsigned int) it->second[i];
      }
   }
   bk_close(pevent, pdata);

   // Make a copy of the ESPER key names.
   // I hate c-style string manipulation
   char *tmp;
   bk_create(pevent, "ESID", TID_CHAR, (void **) &tmp);
   std::string tmp2;
   int counter = 0;
   for (std::map<std::string, int>::iterator it = gData[gModules[2]].i.begin(); it != gData[gModules[2]].i.end(); ++it) {
      char tmp3[10];
      sprintf(tmp3, "%i: ", counter++);
      tmp2 += std::string(tmp3);
      tmp2 += it->first;
      tmp2 += "\n";
   }
   for (std::map<std::string, std::vector<int>>::iterator it = gData[gModules[2]].ia.begin(); it != gData[gModules[2]].ia.end(); ++it) {
      for (unsigned int i = 0; i < it->second.size(); i++) {
         char tmp3[10];
         sprintf(tmp3, "%i: ", counter++);
         tmp2 += std::string(tmp3);
         tmp2 += it->first;
         sprintf(tmp3, "[%i]\n", i);
         tmp2 += std::string(tmp3);
      }
   }

   strncpy(tmp, tmp2.c_str(), tmp2.length() + 1);
   //std::cout << "tmp " << tmp << std::endl;
   tmp += tmp2.length() + 1;
   bk_close(pevent, tmp);

   gEsperComm->Close();

   INT ev_size = bk_size(pevent);
   printf("ev_size: %i\n", ev_size);

   return bk_size(pevent);
}

